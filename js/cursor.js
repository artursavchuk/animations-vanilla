import gsap from 'gsap'

const cursor = document.querySelector('#cursor')
const cursorScale = document.querySelectorAll('[data-cursor-scale]')
let mouseX = 0
let mouseY = 0
const style = getComputedStyle(document.body)
const cursorSize = parseFloat(style.getPropertyValue('--cursor-size'))

gsap.to({}, 0.016, {
  repeat: -1,
  onRepeat: () => {
    gsap.set(cursor, {
      css: {
        left: mouseX,
        top: mouseY,
      },
    })
  },
})

window.addEventListener('mousemove', (e) => {
  mouseX = e.clientX
  mouseY = e.clientY
})

cursorScale.forEach((el) => {
  const scale = el.getAttribute('data-cursor-scale') ?? 1

  el.addEventListener('mousemove', () => {
    cursor.classList.add('active')
    cursor.style.width = cursorSize * scale + 'px'
    cursor.style.height = cursorSize * scale + 'px'
  })

  el.addEventListener('mouseleave', () => {
    cursor.classList.remove('active')
    cursor.style.width = cursorSize + 'px'
    cursor.style.height = cursorSize + 'px'
  })
})
