import '/styles/index.scss'
// import './gui'
import './canvas'
import './animations'
import './cursor'

// smooth scroll to section
const scrollLinks = document.querySelectorAll('[data-scroll-to]')
scrollLinks.forEach((link) => {
  link.addEventListener('click', () => {
    const id = link.getAttribute('data-scroll-to')
    const section = document.querySelector(`#${id}`)
    if (!section) return
    section.scrollIntoView({ behavior: 'smooth' })
    closeMobileMenu()
  })
})

// mobile menu
const burger = document.querySelector('.burger')
burger?.addEventListener('click', () => {
  const menu = document.querySelector('.menu')
  const html = document.querySelector('html')

  burger.classList.toggle('active')
  menu?.classList.toggle('open')
  html.classList.toggle('stop-scrolling')
})

const closeMobileMenu = () => {
  const html = document.querySelector('html')
  const menu = document.querySelector('.menu')
  const burger = document.querySelector('.burger')

  burger?.classList.remove('active')
  menu?.classList.remove('open')
  html.classList.remove('stop-scrolling')
}
