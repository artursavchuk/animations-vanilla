import { GUI } from 'dat.gui'
import { camera, mesh } from './canvas'

const gui = new GUI()

const meshRotation = gui.addFolder('Mesh rotation')
meshRotation.add(mesh.rotation, 'x', -100, 100)
meshRotation.add(mesh.rotation, 'y', -100, 100)
meshRotation.add(mesh.rotation, 'z', -100, 100)
// meshRotation.open()

const meshPosition = gui.addFolder('Mesh position')
meshPosition.add(mesh.position, 'x', -100, 100)
meshPosition.add(mesh.position, 'y', -100, 100)
meshPosition.add(mesh.position, 'z', -100, 100)
// meshPosition.open()

const cameraPosition = gui.addFolder('Camera position')
cameraPosition.add(camera.position, 'x', -300, 300)
cameraPosition.add(camera.position, 'y', -300, 300)
cameraPosition.add(camera.position, 'z', -300, 300)
cameraPosition.open()
