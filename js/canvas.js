import * as THREE from 'three'

let scene, camera, renderer, mesh, material, geometry
const textureString = 'BramblingTech'

const setCanvas = () => {
  // camera
  const fov = 70
  const aspect = window.innerWidth / window.innerHeight
  const near = 1
  const far = 10000
  camera = new THREE.PerspectiveCamera(fov, aspect, near, far)
  camera.position.z = 125
  scene = new THREE.Scene()

  // geometry
  const radius = 30
  const tube = 25
  const rSegments = 50
  const tSegments = 50
  geometry = new THREE.TorusGeometry(radius, tube, rSegments, tSegments)

  // material
  const style = getComputedStyle(document.body)
  const primaryColor = style.getPropertyValue('--text')
  material = new THREE.MeshBasicMaterial({
    color: primaryColor,
    transparent: true,
  })

  // set settings of canvas
  mesh = new THREE.Mesh(geometry, material)
  scene.add(mesh)
  renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true })
  renderer.domElement.id = 'canvas'
  document.body.appendChild(renderer.domElement)
  window.addEventListener('resize', onWindowResize, false)
  onWindowResize()
}

const onWindowResize = () => {
  camera.aspect = window.innerWidth / window.innerHeight
  camera.updateProjectionMatrix()
  renderer.setPixelRatio(window.devicePixelRatio)
  renderer.setSize(window.innerWidth, window.innerHeight)
  setTexture()
  setCameraPosition()
}

const setTexture = () => {
  const canvas = document.createElement('canvas')
  canvas.width = 1024
  canvas.height = 1024

  const context = canvas.getContext('2d')
  context.strokeStyle = '#FFFFFF'
  context.lineWidth = 2
  context.font = '150px Helvetica, Arial'

  for (let i = 0; i < 20; i++) {
    context.strokeText(textureString, 0, i * 171)
  }

  material.map = new THREE.CanvasTexture(
    canvas,
    THREE.UVMapping,
    THREE.RepeatWrapping,
    THREE.RepeatWrapping
  )
}

const setCameraPosition = () => {
  const minWidth = 320
  const maxWidth = 1024
  const minZ = 250
  const maxZ = 170

  const proportionalToWidth =
    ((window.innerWidth - minWidth) / (maxWidth - minWidth)) * (maxZ - minZ) +
    minZ

  camera.position.z = Math.max(proportionalToWidth, maxZ)
  camera.position.y = -20
}

const render = () => {
  material.map.offset.x -= 0.001
  material.map.offset.y += 0.002
  material.map.needsUpdate = true
  mesh.rotation.x += 0.001
  mesh.rotation.y -= 0.001
  mesh.rotation.z += 0.01
  renderer.render(scene, camera)
  requestAnimationFrame(render)
}

const init = () => {
  setCanvas()
  render()
}

init()

export { camera, mesh }
