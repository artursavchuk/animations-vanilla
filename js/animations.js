import gsap from 'gsap'
import { ScrollTrigger } from 'gsap/ScrollTrigger'
import { ScrollToPlugin } from 'gsap/ScrollToPlugin'
import { camera, mesh } from './canvas'

gsap.registerPlugin(ScrollTrigger, ScrollToPlugin)

ScrollTrigger.defaults({
  toggleActions: 'play none none reverse',
})

// ---------- init ----------
const setInitialValues = () => {
  gsap.set(['.hero h1', '.hero p'], { y: 100, opacity: 0 })
}

setInitialValues()

window.addEventListener('load', () => {
  // mesh - from top to normal position
  gsap.from(camera.position, {
    y: -300,
    duration: 0.5,
    ease: 'expo.out',
  })

  // hero section text - from bottom to normal position
  gsap.to('.hero h1', {
    y: 0,
    opacity: 1,
    duration: 2,
    delay: 0.2,
    ease: 'expo.out',
  })
  gsap.to('.hero p', {
    y: 0,
    opacity: 1,
    duration: 2,
    delay: 0.3,
    ease: 'expo.out',
  })
})
// ---------- end of nit ----------

let mm = gsap.matchMedia()

// ---------- mesh (desktop) ----------
mm.add('(width > 1024px)', () => {
  // transition from section hero to section about
  const tl1 = gsap.timeline({
    scrollTrigger: {
      trigger: '.hero',
      start: 'bottom 90%',
      end: 'bottom 70%',
      scrub: true,
      // markers: true,
    },
  })
  tl1.to(mesh.material, { opacity: 0.2 })
  tl1.to(mesh.position, { z: -100 }, '-=0.5')

  const tl2 = gsap.timeline({
    scrollTrigger: {
      trigger: '.hero',
      start: 'bottom 40%',
      end: 'bottom 15%',
      scrub: true,
      // markers: true,
    },
  })
  tl2.to(camera.position, { x: 80 })
  tl2.to(mesh.material, { opacity: 1 }, '-=0.5')
  tl2.to(mesh.position, { z: 0 }, '-=0.5')

  // transition from section about to section info
  const tl3 = gsap.timeline({
    scrollTrigger: {
      trigger: '.info',
      start: 'top 100%',
      end: 'top 50%',
      scrub: true,
      // markers: true,
    },
  })
  tl3.to(mesh.material, { opacity: 0.2 })
  tl3.to(mesh.position, { z: -100 }, '-=0.5')

  const tl4 = gsap.timeline({
    scrollTrigger: {
      trigger: '.info',
      start: 'top 50%',
      end: 'top 25%',
      scrub: true,
      // markers: true,
    },
  })
  tl4.to(camera.position, { x: -80 })
  tl4.to(mesh.material, { opacity: 1 })
  tl4.to(mesh.position, { z: 0 }, '-=0.5')

  // transition from section info to section cards
  const tl5 = gsap.timeline({
    scrollTrigger: {
      trigger: '.info',
      start: 'bottom 90%',
      end: 'bottom 50%',
      scrub: true,
      // markers: true,
    },
  })
  tl5.to(camera.position, { x: -30, y: 0, z: 120 })
})
// ---------- end of mesh (desktop) ----------

// ---------- mesh (mobile) ----------
mm.add('(width <= 1024px)', () => {
  // transition from section hero to section about
  const tl1 = gsap.timeline({
    scrollTrigger: {
      trigger: '.hero',
      start: 'bottom 90%',
      end: 'bottom 70%',
      scrub: true,
      // markers: true,
    },
  })
  tl1.to(mesh.material, { opacity: 0.2 })
  tl1.to(mesh.position, { z: -100 }, '-=0.5')

  const tl2 = gsap.timeline({
    scrollTrigger: {
      trigger: '.hero',
      start: 'bottom 40%',
      end: 'bottom 15%',
      scrub: true,
      // markers: true,
    },
  })
  tl2.to(camera.position, { y: 20, x: 40 })
  tl2.to(mesh.material, { opacity: 0.2 }, '-=0.5')
  tl2.to(mesh.position, { z: 0 }, '-=0.5')

  // transition from section about to section info
  const tl3 = gsap.timeline({
    scrollTrigger: {
      trigger: '.info',
      start: 'top 100%',
      end: 'top 50%',
      scrub: true,
      // markers: true,
    },
  })
  tl3.to(mesh.position, { z: -150 })

  const tl4 = gsap.timeline({
    scrollTrigger: {
      trigger: '.info',
      start: 'top 50%',
      end: 'top 25%',
      scrub: true,
      // markers: true,
    },
  })
  tl4.to(camera.position, { y: 70, x: -40 })
  tl4.to(mesh.position, { z: 0 }, '-=0.5')
})
// ---------- end od mesh (mobile) ----------

// ---------- cards ----------
let cards = gsap.utils.toArray('.card')
cards.forEach((card, i) => {
  ScrollTrigger.create({
    trigger: card,
    start: 'top 95%',
    // markers: true,
    animation: gsap.from(card, {
      autoAlpha: 0,
      y: 100 + 50 * i,
      duration: 1.5,
      ease: 'expo.out',
    }),
  })
})
// ---------- end of cards ----------

// ---------- tabs ----------
const initTabs = () => {
  const buttons = document.querySelectorAll('.tabs__buttons li')
  const contents = document.querySelectorAll('.tab__content')
  const heights = []
  let prev = 0
  let active = 0
  let animation = null
  const tabColor = '#776b5d'
  const activeTabColor = '#fff'

  for (let i = 0; i < buttons.length; i++) {
    buttons[i].index = i
    heights.push(contents[i].offsetHeight)
    gsap.set(contents[i], { top: 0, y: -heights[i] })
    buttons[i].addEventListener('click', () => showTab(i))
  }

  // set initial values
  gsap.set(contents[0], { y: 0 })
  gsap.set('.active-tab-bg', {
    x: buttons[0].offsetLeft,
    width: buttons[0].offsetWidth,
  })
  gsap.set(buttons[0], { color: activeTabColor })
  gsap.set('.tabs__contents', { height: heights[0] })

  const showTab = (current) => {
    if (current === active) return

    if (animation?.isActive()) {
      animation.progress(1)
    }
    animation = gsap.timeline({ defaults: { duration: 0.4 } })
    prev = active
    active = current

    // animate background of active tab
    animation.to('.active-tab-bg', {
      x: buttons[active].offsetLeft,
      width: buttons[active].offsetWidth,
    })

    // change text color on prev and new tab buttons
    animation.to(buttons[prev], { color: tabColor, ease: 'none' }, 0)
    animation.to(buttons[active], { color: activeTabColor, ease: 'none' }, 0)

    // slide current content down out of view and then set it to starting position at top
    animation.to(contents[prev], { y: heights[prev], ease: 'back.in' }, 0)
    animation.set(contents[prev], { y: -heights[prev] })

    // resize container of content
    animation.to('.tabs__contents', { height: heights[active] })

    // slide in new content
    animation.to(
      contents[active],
      { duration: 1, y: 0, ease: 'expo.out' },
      '-=0.25'
    )
  }

  const onResize = () => {
    if (animation?.isActive()) {
      animation.progress(1)
    }

    // set initial state for buttons
    gsap.set(buttons, { color: tabColor })

    // rewrite values of heights
    for (let i = 0; i < contents.length; i++) {
      heights[i] = contents[i].offsetHeight
      gsap.set(contents[i], { y: -heights[i] })
    }

    // set first tab as active
    gsap.set('.active-tab-bg', {
      x: buttons[0].offsetLeft,
      width: buttons[0].offsetWidth,
    })
    gsap.set(buttons[0], { color: activeTabColor })

    // set first content as active
    gsap.set('.tabs__contents', { height: heights[0] })
    gsap.set(contents[0], { y: 0 })
    active = 0
  }
  window.addEventListener('resize', onResize)
}
setTimeout(initTabs, 100)
// ---------- end of tabs ----------
